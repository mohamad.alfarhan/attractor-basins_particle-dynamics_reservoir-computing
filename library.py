import numpy as np
import matplotlib.pyplot as plt
import multiprocessing as multiproc
from joblib import Parallel, delayed
from numba import jit, njit, prange
import sympy as s
import inspect
from matplotlib.animation import FuncAnimation
from library import *
from matplotlib import cm
import matplotlib.colors
from functools import partial
import copy


######		Particle initial conditions		######

@jit
def initilze_particles(N, dimension, method, parameters, x_distribution = lambda q:0,
                 p_distribution = lambda q:0):
    """a function to produce a set of N q_0 coordinates and N p_0 coordinates
    parameters:
        - N: number of particles
        - dimension: dimension of the model
        - method: either 'Von_Neumann' or 'normal'
            use Von_Neumann to draw the coordinates form - x_distribution
                                                         - p_distribution
            using Von Neumann sampling. Or use normal for drawing the coordinates from a normal distribution
        - parameters: normal distribution parameters [mu_q, sigma_p, mu_p, sigma_p]
    
    returns: q_0, p_0 arrays of shape (N, dimension)
    """

    # generate coordinates using Von_Neumann sampling, draw form x_distribution and p_distribution
    # slow method
    if method == 'Von_Neumann':
            n = 0
            q_0 = [] 
            while n < N:
                x_test = np.array([np.random.uniform(-15, 15) for i in range(dimension)])
                y = np.random.uniform(0, 1)
                if y <= x_distribution(x_test):
                        n +=1
                        q_0.append(x_test)
                
            n = 0
            p_0 = [] 
            while n < N:
                p_test = np.array([np.random.uniform(-15, 15) for i in range(dimension)])
                y = np.random.uniform(0, 1)
                if y <= p_distribution(p_test):
                        n +=1
                        p_0.append(p_test)
                
                
            return np.array(q_0), np.array(p_0)

    # generate coordinates using the normal distribution    
    elif method == 'normal':
        
        mu_q = parameters[0]
        sigma_q = parameters[1]
        mu_p = parameters[2]
        sigma_p = parameters[3]
        q_0 = np.random.normal(mu_q,sigma_q,(N,dimension))
        
        cos_theta = np.cos(np.random.uniform(0, 2*np.pi,(N,)))
        sin_theta = np.sin(np.random.uniform(0, 2*np.pi,(N,)))
        cos_phi = np.cos(np.random.uniform(0, np.pi,(N,)))
        sin_phi = np.sin(np.random.uniform(0, np.pi,(N,)))
        v = np.random.normal(mu_p,sigma_p,(N,))
        if dimension == 3:
            v_x = v * cos_theta * sin_phi
            v_y = v * sin_theta * sin_phi
            v_z = v * cos_phi
            p_0 = np.stack((v_x, v_y, v_z), axis=-1)
        elif dimension == 2:
            v_x = v * cos_theta 
            v_y = v * sin_theta 
            p_0 = np.stack((v_x, v_y), axis=-1)
        elif dimension == 1:
            p_0 = v
            
        return np.array(q_0), np.array(p_0)

######		O.D.E. solvers		#######

###	Verlet (2nd Order)	###

#@njit(parallel=True)
def verlet_solver(parameters, model_equations, p_0 , q_0 , dt, num_steps):
    """simple verlet solver to solve a 2nd order ODE
    - parameteres:
        - parameters: parameters of the function model_equations
        - model_equations: a function with retrun value dq/dt, dp/dt (the system ODE)
        - p_0: initial conditions of the momentum
        - q_0: initial conditions of the position
        - dt: time step size of the integration
        - num_steps: number of integration steps
    - returns: q, p arrays of shape ((num_steps, number of particles, dimension of motion))
    """
    # set the mass 
    m = parameters[0]
    N = np.shape(p_0)[0]
    dimension = np.shape(p_0)[1] 
    # Initialize arrays to store trajectories
    q = np.array( [[[0.0 for _ in range(dimension)] for i in range(N)] for _ in range(num_steps)])
    p = np.array( [[[0.0 for _ in range(dimension)] for i in range(N)] for _ in range(num_steps)])
    
    # Set initial conditions
    q[0] = q_0
    p[0] = p_0 
    
    # Perform the Verlet integration
    for i in range(1, num_steps):
        # Compute the half-step momentum
        p_half = p[i-1] + 0.5 * dt * model_equations(parameters, p[i-1],q[i-1],i)[1]
        # Compute the new position
        q[i] = q[i-1] + dt * p_half/m
        # Compute the new momentum
        p[i] = p_half + 0.5 * dt * model_equations(parameters, p_half, q[i],i)[1]
    
    return q, p



@njit(parallel=True)
def sigmoid_verlet_solver(parameters, model_equations, p_0 , q_0 , dt, num_steps, alpha, beta):
    """simple verlet solver to solve a 2nd order ODE
    - parameteres:
        - parameters: parameters of the function model_equations
        - model_equations: a function with retrun value dq/dt, dp/dt (the system ODE)
        - p_0: initial conditions of the momentum
        - q_0: initial conditions of the position
        - dt: time step size of the integration
        - num_steps: number of integration steps
    - returns: q, p arrays of shape ((num_steps, number of particles, dimension of motion))
    """
    # set the mass 
    m = parameters[0]
    N = np.shape(p_0)[0]
    dimension = np.shape(p_0)[1] 
    # Initialize arrays to store trajectories
    q = np.array( [[[0.0 for _ in range(dimension)] for i in range(N)] for _ in range(num_steps)])
    p = np.array( [[[0.0 for _ in range(dimension)] for i in range(N)] for _ in range(num_steps)])
    
    # Set initial conditions
    q[0] = q_0
    p[0] = p_0 
    
    # Perform the Verlet integration
    for i in range(1, num_steps):
        # Compute the half-step momentum
        p_half = p[i-1] + 0.5 * dt * alpha*np.tanh(beta*model_equations(parameters, p[i-1],q[i-1],i)[1])
        # Compute the new position
        q[i] = q[i-1] + dt * p_half/m
        # Compute the new momentum
        p[i] = p_half + 0.5 * dt * alpha*np.tanh(beta*model_equations(parameters, p_half, q[i],i)[1])
    
    return q, p


###	Runge Kutta (1st Order)	###

@njit(parallel=True)
def runge_kutta_solver(parameters, model_equation, q_0, dt, num_steps):
    """simple Runge Kutta solver to solve a 1st order ODE
    - parameteres:
        - parameters: parameters of the function model_equations
        - model_equations: a function with retrun value dq/dt, dp/dt (the system ODE)
        - q_0: initial conditions of the position
        - dt: time step size of the integration
        - num_steps: number of integration steps
    - returns: q  array of shape ((num_steps, number of particles, dimension of motion))
    """

    # find number of particles and dimension
    N = np.shape(q_0)[0]
    dimension = np.shape(q_0)[1]
    # Initialize arrays to store trajectories
    q = np.array( [[[0.0 for _ in range(dimension)] for i in range(N)] for _ in range(num_steps)])
    p = np.array( [[[0.0 for _ in range(dimension)] for i in range(N)] for _ in range(num_steps)])
    # Set initial conditions
    q[0] = q_0
    p[0] = model_equation(parameters, q[0],0)
    # Perform the runge_kutta integration
    for i in range(num_steps-1):
        k1 = model_equation(parameters, q[i],i)
        k2 = model_equation(parameters, q[i] + dt/2 * k1, i)  
        k3 = model_equation(parameters , q[i] + dt/2 * k2, i)
        k4 = model_equation(parameters , q[i] + dt * k3, i)
        q[i+1] = q[i] + (dt/6) * (k1 + 2*k2 + 2*k3 + k4)
        p[i+1] = (k1 + 2*k2 + 2*k3 + k4)/6
    return q, p

######		Physical models		######

###	 Lorenz 84 model 	###

@njit(parallel=True)
def lorenz_84_equation(parameters, q, t):
    """ Lorenz-84 model system equation 
     (Nonlinear Dynamics:A Concise Introduction Interlaced with Code, p.19)
     should have coxisiting attractors for  a = 0.25, b = 4.0, F = 6.886, G = 1.337
    """
    # modelParameters: 
    N = np.shape(q)[0]
    dimension = np.shape(q)[1]    
    a = parameters[0]
    b = parameters[1]
    G = parameters[2]
    R = parameters[3]
    # Compute the derivatives
    dq_dt = np.zeros_like(q)
    for i in prange(N):
        x = q[i,0]
        y = q[i,1]
        z = q[i,2]
        dx = -y**2 -z**2 -a*x +a*R
        dy = x*y -y -b*x*z + G
        dz = b*x*y + x*z -z
        dq_dt[i] = np.array([dx,dy,dz])
    return dq_dt

###	 Lorenz 96 model 	###

@njit(parallel=True)
def lorenz_96_equation(parameters, q, t):
    """ https://en.wikipedia.org/wiki/Lorenz_96_model
    chaotic behaviour for F=8
    """
    # modelParameters: 
    N = np.shape(q)[0]
    dimension = np.shape(q)[1]    
    F = parameters[0]
    
    # Compute the derivatives
    dq_dt = np.zeros_like(q)
    for i in prange(N):
        dq_dt[i] = (q[(i + 1) % N] - q[i - 2]) * q[i - 1] - q[i] + F 
    return dq_dt

###	 particle swarms 	###

#### self propelled with Morse potential
@njit(parallel=True)
def Swarm_eq(parameters, p, q, t):
    N = q.shape[0]
    dimension = q.shape[1]  
    m = parameters[0]
    alpha = parameters[1]
    beta = parameters[2]
    C_r = parameters[3]
    C_a = parameters[4]
    l_r = parameters[5]
    l_a = parameters[6]
    
    gradU = np.zeros((N, dimension))
    dq_dt = np.zeros((N, dimension))
    dp_dt = np.zeros((N, dimension))
    
    # Compute the derivatives
    for i in prange(N):
        for j in prange(N):
            if i != j:
                # Compute repulsion and attraction forces
                repulsion = -C_r / l_r * np.exp(-np.linalg.norm(q[i, :] - q[j, :]) / l_r)
                attraction = C_a / l_a * np.exp(-np.linalg.norm(q[i, :] - q[j, :]) / l_a)
                # Compute combined forces
                force = (repulsion + attraction) * (q[i, :] - q[j, :]) / np.linalg.norm(q[i, :] - q[j, :])
                gradU[i, :] += force
                
    # Compute dq_dt and dp_dt
    for i in prange(N):
        dq_dt[i, :] = p[i, :] / m
        dp_dt[i, :] = (alpha - beta * np.linalg.norm(p[i, :] / m)**2) * p[i, :] / m - gradU[i, :]
    
    return dq_dt, dp_dt


#####
@njit(parallel=True)
def Swarm_homing(parameters, p, q, t):
    N = q.shape[0]
    dimension = q.shape[1]  
    m = parameters[0]
    K_r = parameters[1]
    K_a = parameters[2]
    K_h = parameters[3]
    K_f = parameters[4]
    qh = parameters[5]
    s = parameters[6]
    r_r = parameters[7]
    r_a = parameters[8]
    gradU = np.zeros((N, dimension),dtype=float)
    dq_dt = np.zeros((N, dimension),dtype=float)
    dp_dt = np.zeros((N, dimension),dtype=float)
    
    # Compute the derivatives
    for i in range(N):
        dq_dt[i, :] = p[i, :] / m
        for j in range(N):
            direction = (q[i, :] - q[j, :])
            distance = np.linalg.norm(direction)
            if i != j:
                # F_ri
                if distance < r_r:
                    gradU[i, :] += K_r*direction/distance**2
            # F_ai
            if distance < r_a:
                gradU[i, :] += K_a*(p[j, :] - p[i, :])/ m
        # F_hi
        gradU[i, :] += K_h*(- q[i, :])
        #F_fi
        gradU[i, :] += -K_f*p[i, :]/ m * (np.linalg.norm(p[i, :] / m) - s)/s
                
    return dq_dt, gradU

#####      particle density    #####

@njit
def density(x, t, dx,trajectories):
    """
    a function to calculate the number of particles density
    Args:
        x: position
        t: time is an intger
        dx: volume element size (side length)
        trajectories: list of q[x][t]
    Returns:
        rho: particle number density
    """
    
    # set the sphere Volume
    dimension = np.shape(trajectories)[2]
    if dimension == 2:
        V = np.pi*dx**2
    elif dimension == 3:
        V = 4/3*np.pi*dx*+3
    elif dimension ==1:
        V = 2*dx
    # set initial number of particles
    N = 0
    for q in trajectories:
    
        #compute the number of particles in a box with side-length dx centred around x 
        position = q[t]  
        
        if np.linalg.norm(position-x)<dx:
            
            N +=1
            
    # calculate the density
    rho = N/np.shape(trajectories)[1]/V

    return rho
    
@njit(parallel=True)
def find_densities_(q, dimension, dx):
    lim = np.max(np.abs(q[:, :, :]))
    num_steps = np.shape(q)[0]
    x_arr = np.linspace(-lim,lim,int(2*lim/dx))
    density_arr = np.zeros((int(2*lim/dx),int(2*lim/dx), num_steps))
    for i in prange(len(x_arr)):
        for j in prange(len(x_arr)):
            for t in prange(num_steps):
                    density_arr[i,j,t] = density(np.array([x_arr[i],x_arr[j]]), t, dx, q)
    return density_arr

####        particle system      #####

class particle_system():
    
    def __init__(self, N, dt, num_steps, parameters, model_equations_1st_order = None,\
                 model_equations_2nd_order = None, dimension = 3, sigmoid_activation = None):
        
        # initilze parameters
        self.N = N
        self.dt = dt
        self.num_steps = num_steps
        self.parameters = parameters
        
        #self.model_equations = model_equations
        self.model_equations_1st_order = model_equations_1st_order
        self.model_equations_2nd_order = model_equations_2nd_order
        self.dimension = dimension
        self.sigmoid_activation = sigmoid_activation
        
        # initilze N particles
        mu_q, sigma_p, mu_p, sigma_p = 1.,1.,1.,1.
        self.distribution_parameters = [mu_q, sigma_p, mu_p, sigma_p] 
        self.initilze_particles(dimension , self.distribution_parameters)
        
        # Initialize arrays to store trajectories
        self.q = np.array( [[[0.0 for _ in range(dimension)] for i in range(N)] for _ in range(1)])
        self.p = np.array( [[[0.0 for _ in range(dimension)] for i in range(N)] for _ in range(1)])
        
        # Set initial conditions
        self.q[0] = self.q_0
        self.p[0] = self.p_0 
        self.t = np.array([dt*i for i in range(num_steps)])
        
        # solve the system O.D.E.
        self.evolve(self.num_steps)
        
        # set up the system with no additional particles
        self.t_additional_0 = -1
        
        return
    def load_simulation(self, p, q):
        self.p = p
        self.q = q
        self.num_steps =np.shape(q)[0]
        return
    def initilze_particles(self, dimension, distribution_parameters, method = 'normal'):
        self.q_0, self.p_0 = initilze_particles(self.N, dimension , method , distribution_parameters)
        return 
    
    def evolve(self, num_steps):
        """time evolve the system by the set parameters """
        
        N, dt, parameters = self.N, self.dt, self.parameters
        
        q_0, p_0 = self.q[:,:][-1,:], self.p[:,:][-1,:]
        model_equations_2nd_order = self.model_equations_2nd_order
        model_equations_1st_order = self.model_equations_1st_order
        
        # first order ode, use runga kutta
        if self.model_equations_1st_order != None:
            q, p = runge_kutta_solver(parameters, model_equations_1st_order, q_0 , dt, num_steps)
            self.q = np.concatenate((self.q, q[1:]), axis=0)
            self.p = np.concatenate((self.p, p[1:]), axis=0)
            
        elif self.model_equations_2nd_order != None:
            if  self.sigmoid_activation == None:
                q, p = verlet_solver(parameters, model_equations_2nd_order , p_0 , q_0, dt, num_steps)
            else:
                alpha = self.sigmoid_activation[0]
                beta = self.sigmoid_activation[1]
                q, p = sigmoid_verlet_solver(parameters, model_equations_2nd_order , p_0 , q_0, dt, num_steps, alpha, beta)
            self.q = np.concatenate((self.q, q[1:]), axis=0)
            self.p = np.concatenate((self.p, p[1:]), axis=0)
        return
    
    def evolve_with_additional_particles(self, q_additional, force, force_type, force_parameters, num_steps = -1):
        # get system parameters
        N, dt, parameters = self.N, self.dt, self.parameters
        self.t_additional_0 = np.shape(self.q)[0]
        self.q_additional = q_additional
        model_equations_2nd_order = self.model_equations_2nd_order
        model_equations_1st_order = self.model_equations_1st_order
        
        # get the current state of the system
        q_0, p_0 = self.q[:,:][-1,:], self.p[:,:][-1,:]
        if num_steps == -1:
            num_steps = np.shape(q_additional)[0]
        
        # combine the system with the additional particles to calculate the total force
        if model_equations_1st_order != None:
            @njit(parallel=True)
            def combined_system_1st_order(parameters, q, t): 
                return model_equations_1st_order(parameters, p, q, t) + \
                              additional_particles_interaction(force, force_type, force_parameters, q, q_additional, t)
            q, p = runge_kutta_solver(parameters, combined_system_1st_order, q_0 , dt, num_steps)
            # store the total system 
            self.q = np.concatenate((self.q, q[1:]), axis=0)
            self.p = np.concatenate((self.p, p[1:]), axis=0)
        if model_equations_2nd_order != None:
            #@njit(parallel=True)
            def combined_system_2nd_order(parameters, p, q, t):
                return model_equations_2nd_order(parameters, p, q,t) + additional_particles_interaction(force, force_type, force_parameters, p, q, q_additional, t)
            if  self.sigmoid_activation == None:
                q, p = verlet_solver(parameters, combined_system_2nd_order , p_0 , q_0, dt, num_steps)
            else:
                alpha = self.sigmoid_activation[0]
                beta = self.sigmoid_activation[1]
                q, p = sigmoid_verlet_solver(parameters, combined_system_2nd_order , p_0 , q_0, dt, num_steps,alpha,beta)
            # store the total system 
            self.q = np.concatenate((self.q, q[1:]), axis=0)
            self.p = np.concatenate((self.p, p[1:]), axis=0)
        
        
        return
            
    def find_densities(self, dx):
        dimension = self.dimension
        q = copy.copy(self.q)
        lim = np.max(np.abs(q[:, :, :]))
        num_steps = np.shape(q)[0]
        x_arr = np.linspace(-lim,lim,int(2*lim/dx))
        if dimension == 2:
            self.density = find_densities_(q, dimension, dx)
            self.X, self.Y = np.meshgrid(x_arr, x_arr)  
        elif dimension == 3:
            self.density = find_densities_(q, dimension, dx)
            self.X, self.Y, self.Z = np.meshgrid(x_arr, x_arr, x_arr) 
        elif dimension == 1:
            self.density = find_densities_(q, dimension, dx)
            self.X = x_arr
            
    def find_center_of_mass(self):
        self.center_of_mass = np.zeros((np.shape(self.q)[0], np.shape(self.q)[2]))
        for t in range(np.shape(self.q)[0]):
            for d in range( np.shape(self.q)[2]):
                self.center_of_mass[t,d] = np.mean(self.q[t,:,d])
        return
   
    def find_average_velocity(self):
        self.average_velocity = np.zeros((np.shape(self.q)[0], np.shape(self.q)[2]))
        for t in range(np.shape(self.q)[0]):
            for d in range( np.shape(self.q)[2]):
                self.average_velocity[t,d] = np.mean(self.p[t,d])
        return
    
    def enable_observation_layers(self, M):
        ob = observation_layer(M, self.q[self.t_additional_0:,:,:], self.p[self.t_additional_0:,:,:])
        self.ob = ob
        self.layer_1 = ob.layer_1
        self.layer_2 = ob.layer_2
        self.layer_3 = ob.layer_3
        return

    def animate(self, show_density = False, show_center_of_mass = False,\
                percentage_of_particles = .4, arrow_length = 1, s = 0.5, xlim = None, ylim = None,\
               xlim_lower = None, ylim_lower = None, zlim_lower = None, zlim = None, show_axis = False):
        """produce an animation of the system,
        parameters: - percentage_of_particles to be shown in the animation"""
        if show_density:
            fig, ax = plt.subplots()  
            self.ax = ax
            norm = cm.colors.Normalize(vmax=abs(self.density).max(), vmin=abs(self.density).min())
            def animate_frame(i):
                ax.clear()
                ax.set_xlabel("x")
                ax.set_ylabel("y")
                contour_lines = ax.contour(self.Y, self.X, self.density[:, :, i], cmap='plasma')
                plt.clabel(contour_lines, inline=True, fontsize=8)
                ax.contourf(self.Y, self.X, self.density[:, :, i], 40,norm=norm)
        else:
            # select particles to plot
            number_of_particles = self.q.shape[1]
            particles_to_plot = int(number_of_particles * percentage_of_particles)
            selected_particles = np.random.choice(number_of_particles, particles_to_plot, replace=False)
            # scale arrow length 
            q = self.q[:, selected_particles, :]
            p = self.p[:, selected_particles, :]
            norms = np.linalg.norm(p, axis=2, keepdims=True)
            safe_norms = np.where(norms > 1e-5, norms, np.ones_like(norms))
            p_scaled = np.divide(p, safe_norms, out=np.zeros_like(p), where=safe_norms > 1e-5) * arrow_length
            
            # create figure with correct projection
            if self.dimension == 3:
                fig = plt.figure()
                ax = fig.add_subplot(projection='3d')
            else:
                fig, ax = plt.subplots()  
            # get axis limits
            self.ax = ax
            if self.dimension == 3:
                if xlim == None:
                    xlim = np.max(np.abs(self.q[:, :, 0]))
                if ylim == None:
                    ylim = np.max(np.abs(self.q[:, :, 1]))
                if xlim_lower == None:
                    xlim_lower = - xlim
                if ylim_lower == None:
                    ylim_lower = - ylim
                if zlim == None:
                    zlim = np.max(np.abs(self.q[:, :, 2]))
                if zlim_lower == None:
                    zlim_lower = - zlim
                def animate_frame(i):
                    ax.clear()
                    ax.set_xlabel("$x$")
                    ax.set_ylabel(r'$y$')
                    ax.set_zlabel(r'$z$')
                    if show_center_of_mass:
                        ax.scatter(self.center_of_mass[i,0], self.center_of_mass[i,1], color ='g', label="center of mass", s=0.1)
                    if self.t_additional_0 !=-1:
                        if i >self.t_additional_0:
                            for n in range(np.shape(self.q_additional)[1]):
                                ax.scatter(self.q_additional[:, n][i-self.t_additional_0, 0],\
                                           self.q_additional[:, n][i-self.t_additional_0, 1],\
                                           self.q_additional[:, n][i-self.t_additional_0, 2],color='r',s=s)
                    for n in range(len(selected_particles)):
                        ax.scatter(q[:, n][i, 0], q[:, n][i, 1],q[:, n][i, 2], color ='b',s=s)
                        ax.quiver(q[:, n][i, 0], q[:, n][i, 1],q[:, n][i, 2],\
                                  p_scaled[:, n][i, 0], p_scaled[:, n][i, 1],p_scaled[:, n][i, 2],\
                                  color='k', length=arrow_length, normalize=True,pivot='middle') 
                        
        
                    ax.set_xlim(xlim_lower, xlim)
                    ax.set_ylim(ylim_lower,ylim)
                    ax.set_zlim(zlim_lower, zlim)
                    
            elif self.dimension == 2:
                if xlim == None:
                    xlim = np.max(np.abs(self.q[:, :, 0]))*1.3
                if ylim == None:
                    ylim = np.max(np.abs(self.q[:, :, 1]))*1.3
                if xlim_lower == None:
                    xlim_lower = - xlim
                if ylim_lower == None:
                    ylim_lower = - ylim
                def animate_frame(i):
                    ax.clear()
                    ax.set_xlabel("$x$")
                    ax.set_ylabel(r'$y$')
                    if show_center_of_mass:
                        ax.scatter(self.center_of_mass[i,0], self.center_of_mass[i,1], color ='g', label="center of mass",s=s)
                    if self.t_additional_0 !=-1:
                        if i >self.t_additional_0:
                            for n in range(np.shape(self.q_additional)[1]):
                                ax.scatter(self.q_additional[:, n][i-self.t_additional_0, 0],\
                                           self.q_additional[:, n][i-self.t_additional_0, 1], color='r', s = s)
                    for n in range(len(selected_particles)):
                        ax.scatter(q[:, n][i, 0], q[:, n][i, 1], color ='b',s=s)
                        ax.quiver(q[:, n][i, 0], q[:, n][i, 1],\
                                  p_scaled[:, n][i, 0], p_scaled[:, n][i, 1],\
                                  color='k', scale=6, scale_units='inches', angles='uv', pivot='mid',headwidth=3, width= .003) 
        
                    ax.set_xlim(xlim_lower, xlim)
                    ax.set_ylim(ylim_lower,ylim)
                    if not show_axis:
                        ax.axis('off') 
    
        self.ani = FuncAnimation(fig, animate_frame, frames=np.shape(self.q)[0], interval=self.dt, repeat=False)
        plt.show()
       
        
    
##### observation layer  #####


class observation_layer():
    def __init__(self, M, q, p):
        self.M = M
        self.q = q
        self.p = p
        self.dimension = np.shape(q)[2]
        self.initialize_kernals()
        return
    
    def initialize_kernals(self):
        mu_c, sigma_c, mu_w, sigma_w = 1.,1.,1.,1.
        self.cm = np.random.normal(mu_c,sigma_c,(self.M,self.dimension))
        self.wm = np.random.normal(mu_w,sigma_w,self.M)
        self.kernals = [lambda q: np.exp(-(q-self.cm[i])@(q-self.cm[i])/(2*self.wm[i])) for i in range(self.M)]
        self.generate_layers()
        return
        
    def generate_layer_1(self):
        self.layer_1 = np.zeros((np.shape(self.q)[0],self.M))
        for t in range(np.shape(self.q)[0]):
            for k in range(self.M):
                kernal = self.kernals[k]
                for n in range(np.shape(self.q)[1]):
                    self.layer_1[t,k] += kernal(self.q[t,n,:])
        return
    
    def generate_layer_2(self):
        self.layer_2 = np.zeros((np.shape(self.q)[0],self.M))
        for t in range(np.shape(self.q)[0]):
            for k in range(self.M):
                kernal = self.kernals[k]
                for n in range(np.shape(self.q)[1]):
                    self.layer_2[t,k] += kernal(self.q[t,n,:])*self.p[t,n,0]
        return
    
    def generate_layer_3(self):
        self.layer_3 = np.zeros((np.shape(self.q)[0],self.M))
        for t in range(np.shape(self.q)[0]):
            for k in range(self.M):
                kernal = self.kernals[k]
                for n in range(np.shape(self.q)[1]):
                    self.layer_3[t,k] += kernal(self.q[t,n,:])*self.p[t,n,1]
        return
    
    def generate_layers(self):
        
        self.generate_layer_1()
        self.generate_layer_2()
        self.generate_layer_3()
        
        return
    
    def plot(self, m = 0,layer_1 = False,layer_2 = False, layer_3 = False, all_layers = True):
        lines = ['dashdot','dashed','dotted']
        colors = ['tab:blue','tab:purple','darkseagreen']
        fig, ax = plt.subplots()  
        self.ax = ax
        if layer_1 or all_layers:
            ax.plot([i for i in range(np.shape(self.q)[0])], self.layer_1[:,m],\
                    label = 'layer 1, m = '+ str(m), linestyle = lines[0], color = colors[0])
        if layer_2 or all_layers:
            ax.plot([i for i in range(np.shape(self.q)[0])], self.layer_2[:,m],\
                    label = 'layer 2, m = '+ str(m), linestyle = lines[1], color = colors[1])
        if layer_3 or all_layers:
            ax.plot([i for i in range(np.shape(self.q)[0])], self.layer_3[:,m],\
                    label = 'layer 3, m = '+ str(m), linestyle = lines[2], color = colors[2])
        ax.set_ylabel(ylabel="observation layers", fontsize = 14.)
        
        ax.set_xlabel(xlabel="time step", fontsize = 14.)
        ax.label_outer()
        ax.grid()
        ax.legend()
        plt.show()
        return
                                
                     
            

##### interaction with additional particles  ######

#@njit(parallel=True)
def additional_particles_interaction(force, force_type, force_parameters, p, q, q_additional, t):
    """calculats the force on q due to the presence of q_additional partilces"""
    # modelParameters: 
    N = np.shape(q)[0]
    N_additional = np.shape(q_additional)[1]
    dimension = np.shape(q)[1]
    interaction_range = 5
    q_additional = q_additional[t,:,:]
    dp_dt = np.zeros((N, dimension))
        
    # Compute the force
    dq_dt = np.zeros_like(q)
    for i in prange(N):
        dq_dt[i, :] = p[i, :]
        for j in prange(N_additional):
            distance = np.linalg.norm(q[i, :] - q_additional[j, :])
            if force_type == 'attractive':
                direction = q[i, :] - q_additional[j, :]
            else: # force_type == 'repulsive': 
                direction = -(q[i, :] - q_additional[j, :])
            dp_dt[i, :] = force(direction, distance, force_parameters)
    return dq_dt, dp_dt


#@njit
def force_1(direction, distance, force_parameters):
    r_r = force_parameters[0]
    K_r = force_parameters[1]
    if distance < r_r:
        heavy_side = 1
    else:
        heavy_side = 0
    return K_r*heavy_side* direction/distance**2

#@njit
def force_2(direction, distance, force_parameters):
    C_r = force_parameters[0]
    l_r = force_parameters[1]
    factor = C_r / l_r * np.exp(-distance / l_r)
    return factor * direction / distance

