import numpy as np
import scipy

import matplotlib.pyplot as plt

from tqdm import tqdm

class Reservoir:
    def __init__(self, groundtruth, data, times, axis_label=None):
        self.alpha = 0.7
        self.beta  = 1e-10

        self.n_nodes  = 50
        self.sparsity = 0.1
        self.target_spectral_radius = 1
        self.bias_scaling  = 1
        self.input_scaling = 0.73

        self.prob = 0.1

        self.hist = 1
        self.axis_label = axis_label

        self.inputs=data.shape[0]*self.hist+1

        self.warmup_time, self.train_time, self.test_time = times[0], times[1], times[2]
        self.time = self.warmup_time + self.train_time + self.test_time
        
        self.nodes = np.random.random(self.n_nodes)

        np.random.seed(42)
        self.weights_in  = np.random.uniform(low=-self.prob, high=self.prob, size=(self.n_nodes, self.inputs))
        self.weights_in[0,:]   /= self.bias_scaling
        self.weights_in[1:,:]  /= self.input_scaling
        self.weights     = scipy.sparse.random(self.n_nodes, self.n_nodes, self.sparsity, random_state=42, data_rvs=scipy.stats.uniform(loc=-self.prob, scale=2*self.prob).rvs).toarray()
        self.weights_out = None
        self.weights_fb = np.random.uniform(low=-self.prob, high=self.prob, size=(self.n_nodes, data.shape[0]))
        
        self.groundtruth = groundtruth
        self.data = np.copy(data)
        self.observables = np.zeros((self.n_nodes, self.time))
        self.readout = np.zeros((data.shape[0], self.time))
        self.error = 0
        self.t = 0

        # normalize and scale spectral radius
        spectral_radius = np.max(np.absolute(np.linalg.eigvals(self.weights)))
        self.weights /= spectral_radius
        self.weights *= self.target_spectral_radius

    def reset(self):
        self.inputs = self.data.shape[0]*self.hist+1
        self.nodes = np.random.random(self.n_nodes)

        np.random.seed(42)
        self.weights_in = np.random.uniform(low=-self.prob, high=self.prob, size=(self.n_nodes, self.inputs))
        self.weights_in[0,:]   /= self.bias_scaling
        self.weights_in[1:,:]  /= self.input_scaling
        self.weights     = scipy.sparse.random(self.n_nodes, self.n_nodes, self.sparsity, random_state=42, data_rvs=scipy.stats.uniform(loc=-self.prob, scale=2*self.prob).rvs).toarray()
        self.weights_out = None
        self.weights_fb  = np.random.uniform(low=-self.prob, high=self.prob, size=(self.n_nodes, self.data.shape[0]))

        self.observables = np.zeros((self.n_nodes, self.time))
        self.readout = np.zeros((self.data.shape[0], self.time))

        # normalize and scale spectral radius
        spectral_radius = np.max(np.absolute(np.linalg.eigvals(self.weights)))
        self.weights /= spectral_radius
        self.weights *= self.target_spectral_radius
        self.t = 0
        
    def update(self, input, feedback):
        update = self.weights_in @ input + self.weights @ self.nodes + self.weights_fb @ feedback
        self.nodes = self.alpha * np.tanh(update) + (1 - self.alpha) * self.nodes

    def warmup(self):
        self.t = self.hist-1
        for j in range(self.hist,self.warmup_time):
            self.update(np.concatenate(([1], self.data[:,self.t-(self.hist-1):self.t+1].flatten())), self.groundtruth[:, self.t])
            self.observables[:, self.t] = self.nodes
            self.t += 1

    def train(self):
        for j in range(self.train_time):
            self.update(np.concatenate(([1], self.data[:,self.t-(self.hist-1):self.t+1].flatten())), self.groundtruth[:,self.t-1])
            self.observables[:, self.t] = self.nodes
            self.t += 1

        self.calc_weights()
        #self.readout[:, self.warmup_time:self.warmup_time+self.train_time] = np.vstack(self.weights_out) @ np.vstack((self.observables[:,self.warmup_time:self.warmup_time+self.train_time], np.ones(self.train_time), self.data[:,self.warmup_time:self.warmup_time+self.train_time]))
        self.readout[:, self.warmup_time:self.warmup_time+self.train_time] = np.vstack(self.weights_out) @ self.observables[:,self.warmup_time:self.warmup_time+self.train_time]


    def test(self):
        for j in range(self.test_time+1):
            self.update(np.concatenate(([1], self.readout[:,self.t-self.hist:self.t].flatten())), self.readout[:, self.t-1])
            self.observables[:, self.t] = self.nodes
            #self.readout[:, self.t] = self.weights_out @ np.concatenate((self.observables[:,self.t], [1], self.readout[:, self.t-1]))
            self.readout[:, self.t] = self.weights_out @ self.observables[:,self.t]
            self.t += 1

        self.error = np.zeros(self.test_time)
        for j in range(self.test_time):
            self.error[j] = ((self.groundtruth[:,self.warmup_time+self.train_time+j] - self.readout[:,self.warmup_time+self.train_time+j])**2).mean()

    def calc_weights(self):
        x = self.observables[:,self.warmup_time:self.warmup_time+self.train_time]
        #x = np.vstack((x, np.ones(x.shape[1]), self.data[:,self.warmup_time:self.warmup_time+self.train_time]))
        design_mat = x @ x.T
        #tikhunov_reg = self.beta * np.eye(self.n_nodes + self.data.shape[0]+1)   
        tikhunov_reg = self.beta * np.eye(self.n_nodes)   
        y_target = self.groundtruth[:,self.warmup_time:self.warmup_time+self.train_time]
        self.weights_out = (y_target @ x.T) @ np.linalg.pinv(design_mat + tikhunov_reg)

    def plot_results(self):
        # plot train and test performance
        fig, axs = plt.subplots(self.data.shape[0], 2, figsize=(15, 2.5*(self.data.shape[0])))

        for i in range(self.data.shape[0]):        
            axs[i,0].plot(self.groundtruth[i, self.warmup_time:self.warmup_time+self.train_time])
            axs[i,0].plot(self.readout[i, self.warmup_time:self.warmup_time+self.train_time])
            axs[i,1].plot(self.groundtruth[i, self.warmup_time+self.train_time:self.time])
            axs[i,1].plot(self.readout[i, self.warmup_time+self.train_time:self.time])
            if self.axis_label != None:
                axs[i,0].set_ylabel(self.axis_label[i])
                axs[i,1].set_ylabel(self.axis_label[i])
        axs[0,0].set_title("training performance")
        axs[0,1].set_title("test performance")
        plt.show()

        # plot error and observables
        fig, axs = plt.subplots(1,2,figsize=(15,5))
        
        axs[0].plot(self.error)
        axs[0].set_title("error")
        
        axs[1].plot(self.observables.T)
        axs[1].set_title("observables")
        plt.show()

    def optimize_parameters(self):
        def optimization_loop(start, stop, width, param, iterations):
            best_param = 0
            best_error = 1e15
            for j in tqdm(range(iterations)):
                param_ls = np.arange(start, stop+width, width)
                for i in range(param_ls.shape[0]):
                    vars(self)[param] = param_ls[i]
                    self.reset()
                    self.warmup()
                    self.train()
                    self.test()
                    if np.sum(self.error) < best_error:
                        best_error = np.sum(self.error)
                        best_param = param_ls[i]
                if best_param == stop:
                    print("hitting upper boundary")                    
                if best_param == start and j != 0:
                    print("hitting lower boundary")
                start, stop = best_param - width, best_param + width
                width /= 10
                if start == 0.:
                    start += width
            vars(self)[param] = best_param
            #print(vars(self)[param] , best_param)

        optimization_loop(10, 200, 10, "input_scaling", 2)        
        optimization_loop(10, 200, 10, "bias_scaling", 2)
        optimization_loop(0.5, 0.9, 0.1, "target_spectral_radius", 2)
        optimization_loop(0.1, 0.9, 0.1, "alpha", 2)
        optimization_loop(0.1, 0.9, 0.1, "prob", 2)
        
        best_error = 1e15
        self.beta = 1e-5
        for i in tqdm(range(7)):
            self.reset()
            self.warmup()
            self.train()
            self.test()
            if np.sum(self.error) < best_error:
                best_error = np.sum(self.error)
                param_set = self.beta
                #print(best_error, param_set)
            self.beta *= 10
        #print(param_set)
        self.beta = param_set